from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy

from tasks.models import Task

# from projects.models import Project


# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee", "notes"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/list.html"
    fields = ["is_completed"]

    def form_valid(self, form):
        task = form.save(commit=False)
        task.save()
        return redirect("show_my_tasks")


class TaskDetailView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = "tasks/detail.html"


class TaskEditView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/edit.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "assignee",
        "notes",
    ]

    def get_success_url(self):
        return reverse_lazy("details_task", args=[self.object.id])


class TaskDeleteView(LoginRequiredMixin, DeleteView):
    model = Task
    template_name = "tasks/delete.html"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])
